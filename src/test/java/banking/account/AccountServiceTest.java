package banking.account;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import banking.api.ApiException;
import banking.api.ErrorCode;
import banking.operation.OperationService;
import banking.operation.OperationType;
import banking.user.UserService;
import java.math.BigDecimal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {

  private static final long ID = 99L;
  private static final long ACCOUNT_ID = 9898L;
  private static final long NON_EXISTENT_ID = 888L;
  private static final BigDecimal INITIAL_AMOUNT = BigDecimal.TEN;
  private static final BigDecimal FEE = BigDecimal.ONE;
  @Mock
  private AccountRepo accountRepo;

  @Mock
  private UserService userService;

  @Mock
  private OperationService operationService;

  @InjectMocks
  private AccountService accountService;

  @Captor
  private ArgumentCaptor<AccountRequest> accountRequestCaptor;

  @BeforeEach
  void setup() {
    when(userService.userExists(anyLong())).thenReturn(true);
    lenient().when(operationService.getFee(OperationType.ACCOUNT_CREATION_FEE, INITIAL_AMOUNT))
        .thenReturn(BigDecimal.ZERO);
  }

  @Test
  void create_ShouldInvokeRepoCreate() {
    AccountRequest accountRequest = withAccountRequest();

    accountService.create(ID, accountRequest);

    verify(accountRepo).create(ID, accountRequest);
  }

  @Test
  void create_ShouldInvokeRepoFind() {
    AccountRequest accountRequest = withAccountRequest();
    when(accountRepo.create(ID, accountRequest)).thenReturn(ACCOUNT_ID);

    accountService.create(ID, accountRequest);

    verify(accountRepo).find(ACCOUNT_ID);
  }

  @Test
  void create_ShouldCheckUser() {

    accountService.create(ID, withAccountRequest());

    verify(userService).userExists(ID);
  }

  @Test
  void create_ShouldThrowException_WhenUserNotFound() {
    when(userService.userExists(NON_EXISTENT_ID)).thenReturn(false);

    ApiException apiException = assertThrows(ApiException.class,
        () -> accountService.create(NON_EXISTENT_ID, withAccountRequest()));

    assertEquals(ErrorCode.E002, apiException.getErrorCode());
  }

  @Test
  void create_ShouldReturnAccountResponse() {
    AccountRequest accountRequest = withAccountRequest();
    when(accountRepo.create(ID, accountRequest)).thenReturn(ACCOUNT_ID);
    when(accountRepo.find(ACCOUNT_ID)).thenReturn(new AccountResponse());

    AccountResponse accountResponse = accountService.create(ID, accountRequest);

    assertNotNull(accountResponse);
  }

  @Test
  void create_ShouldThrowException_WhenInitialAmountIsNull() {
    ApiException apiException = assertThrows(ApiException.class,
        () -> accountService.create(ID, new AccountRequest()));
    assertEquals(ErrorCode.E003, apiException.getErrorCode());
  }

  @Test
  void create_ShouldInvokeFeeService() {
    accountService.create(ID, withAccountRequest());

    verify(operationService).getFee(OperationType.ACCOUNT_CREATION_FEE, INITIAL_AMOUNT);
  }

  @Test
  void create_ShouldInvokeFeeServiceApplyFee() {
    AccountRequest accountRequest = withAccountRequest();
    when(accountRepo.create(ID, accountRequest)).thenReturn(ACCOUNT_ID);

    accountService.create(ID, accountRequest);

    verify(operationService)
        .operation(ACCOUNT_ID, BigDecimal.ZERO, OperationType.ACCOUNT_CREATION_FEE);
  }

  private AccountRequest withAccountRequest() {
    AccountRequest accountRequest = new AccountRequest();
    accountRequest.setAmount(INITIAL_AMOUNT);
    return accountRequest;
  }

}