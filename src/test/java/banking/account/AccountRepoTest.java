package banking.account;

import static org.junit.jupiter.api.Assertions.assertEquals;

import banking.config.BindingModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AccountRepoTest {

  private static final BigDecimal AMOUNT = new BigDecimal(100.65);
  private QueryRunner runner;
  private AccountRepo accountRepo;
  private long userId;

  @BeforeEach
  void setup() throws SQLException {
    Injector injector = Guice.createInjector(new BindingModule());
    runner = new QueryRunner(injector.getInstance(DataSource.class));
    accountRepo = injector.getInstance(AccountRepo.class);
    runner.update("delete from operations");
    runner.update("delete from accounts");
    userId = runner.insert("insert into users(first_name, last_name) values ('test', 'test')",
        new ScalarHandler<>());
  }

  @Test
  void create_ShouldAddRecordInDb() throws SQLException {
    AccountRequest accountRequest = new AccountRequest();
    accountRequest.setAmount(AMOUNT);
    long id = accountRepo.create(userId, accountRequest);

    long count = runner
        .query("select count(*) from accounts where id = ?", new ScalarHandler<>(), id);

    assertEquals(1, count);
  }

  @Test
  void find_ShouldReturnResponseIfFound() throws SQLException {
    long id = runner
        .insert("insert into accounts (owner_id, amount) values(?, ?)", new ScalarHandler<>(),
            userId,
            AMOUNT);

    AccountResponse accountResponse = accountRepo.find(id);

    assertEquals(id, accountResponse.getId());
  }

  @Test
  void find_ShouldCorrectlySaveAmount() throws SQLException {
    long id = runner
        .insert("insert into accounts (owner_id, amount) values(?, ?)", new ScalarHandler<>(),
            userId,
            AMOUNT);

    AccountResponse accountResponse = accountRepo.find(id);

    assertEquals(AMOUNT.setScale(2, RoundingMode.HALF_UP),
        accountResponse.getAmount());
  }
}