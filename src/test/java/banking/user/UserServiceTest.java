package banking.user;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import banking.api.ApiException;
import banking.api.ErrorCode;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

  @Mock
  private UserRepo userRepo;

  @InjectMocks
  private UserService userService;

  @Test
  void create_ShouldThrowError_WhenFirstAndLastNameAreEmpty() {
    ApiException apiException = assertThrows(ApiException.class, () -> {
      userService.create(new UserRequest());
    });

    assertEquals(ErrorCode.E001, apiException.getErrorCode());

  }

}