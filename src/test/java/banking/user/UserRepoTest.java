package banking.user;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import banking.config.BindingModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class UserRepoTest {

  private QueryRunner runner;
  private UserRepo userRepo;

  @BeforeEach
  void setup() throws SQLException {
    Injector injector = Guice.createInjector(new BindingModule());
    runner = new QueryRunner(injector.getInstance(DataSource.class));
    userRepo = injector.getInstance(UserRepo.class);
    runner.update("delete from operations");
    runner.update("delete from accounts");
    runner.update("delete from users");
  }

  @Test
  void create_ShouldAddRecordInDb() throws SQLException {
    long id = userRepo.create(new UserRequest());

    long count = runner.query("select count(*) from users where id = ?", new ScalarHandler<>(), id);

    assertEquals(1, count);
  }


  @Test
  void userExists_ShouldReturnTrue_WhenUserExists() throws SQLException {
    long id = runner.insert("insert into users(first_name, last_name) values ('test', 'test')",
        new ScalarHandler<>());

    assertTrue(userRepo.userExists(id));
  }

  @Test
  void userExists_ShouldReturnFalse_WhenUserDoesNotExist() throws SQLException {
    long fakeId = -999;

    assertFalse(userRepo.userExists(fakeId));
  }
}