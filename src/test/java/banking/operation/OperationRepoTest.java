package banking.operation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import banking.config.BindingModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class OperationRepoTest {

  private QueryRunner runner;
  private OperationRepo operationRepo;
  private long userId;
  private long accountId;

  @BeforeEach
  void setup() throws SQLException {
    Injector injector = Guice.createInjector(new BindingModule());
    runner = new QueryRunner(injector.getInstance(DataSource.class));
    operationRepo = injector.getInstance(OperationRepo.class);
    runner.update("delete from accounts");
    userId = runner.insert("insert into users(first_name, last_name) values ('test', 'test')",
        new ScalarHandler<>());
    accountId = runner
        .insert("insert into accounts (owner_id, amount) values(?, 0.0)", new ScalarHandler<>(),
            userId);
  }

  @Test
  public void balance_ShouldReturnCorrectAmount() throws SQLException {
    runner.update("insert into operations (account_id, amount, operation_type) values (?, ?, ?)",
        accountId, new BigDecimal(100), OperationType.INIT_ACCOUNT.toString());
    runner.update("insert into operations (account_id, amount, operation_type) values (?, ?, ?)",
        accountId, new BigDecimal(-11), OperationType.OTHER.toString());

    List<BalanceResponse> balanceResponses = operationRepo.balanceByOwner(userId);

    assertEquals(new BigDecimal(89).setScale(2, RoundingMode.HALF_UP),
        balanceResponses.get(0).getAccountBalance());
  }

}