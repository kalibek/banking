package banking.operation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class OperationServiceTest {

  @InjectMocks
  private OperationService operationService;

  @Test
  void getFee_ShouldReturnOnePercentOfAmount_WhenOperationIsAccountCreation() {
    BigDecimal fee = operationService.getFee(OperationType.ACCOUNT_CREATION_FEE, new BigDecimal(1500));

    assertEquals(new BigDecimal(15), fee);

  }

  @Test
  void getFee_ShouldReturnNotLessThenFive_WhenFeeIsLessThenFiveAndOperationIsAccountCreation() {
    BigDecimal fee = operationService.getFee(OperationType.ACCOUNT_CREATION_FEE, new BigDecimal(10));

    assertEquals(new BigDecimal(5), fee);
  }

  @Test
  void getFee_ShouldReturnNotMoreThenThirty_WhenFeeIsGreaterThenThirtyAndOperationIsAccountCreation() {
    BigDecimal fee = operationService.getFee(OperationType.ACCOUNT_CREATION_FEE, new BigDecimal(35000));

    assertEquals(new BigDecimal(30), fee);
  }

  @Test
  void getFee_ShouldReturnZero_WhenOperationIsNotAccountCreation() {
    BigDecimal fee = operationService.getFee(OperationType.OTHER, new BigDecimal(10000000000000000L));

    assertEquals(BigDecimal.ZERO, fee);
  }

}