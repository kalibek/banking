package banking.integration;

import static banking.integration.IntegrationUtils.load;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

import banking.Application;
import banking.config.BindingModule;
import banking.operation.OperationType;
import com.google.inject.Guice;
import com.google.inject.Injector;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CreateAccountIntegrationTest {

  private static final String TEST = "TEST";
  private static RequestSpecification spec = new RequestSpecBuilder()
      .setBaseUri("http://localhost:8008")
      .setBasePath("/api/v1/")
      .build();

  private long userId;

  @BeforeAll
  static void start() {
    Application.main(null);
  }

  @AfterAll
  static void stop() {
    Application.stopServer();
  }

  @BeforeEach
  void setup() throws SQLException {
    Injector injector = Guice.createInjector(new BindingModule());
    QueryRunner runner = new QueryRunner(injector.getInstance(DataSource.class));
    userId = runner.insert("insert into users(first_name, last_name) values ('test', 'test')",
        new ScalarHandler<>());
    long accountId = runner
        .insert("insert into accounts (owner_id, amount) values(?, 0.0)", new ScalarHandler<>(),
            userId);
    runner.update("insert into operations (account_id, amount, operation_type) values (?, ?, ?)",
        accountId, new BigDecimal(100), OperationType.INIT_ACCOUNT.toString());
    runner.update("insert into operations (account_id, amount, operation_type) values (?, ?, ?)",
        accountId, new BigDecimal(-11), OperationType.OTHER.toString());
  }

  @Test
  void shouldCreateUser() throws IOException {
    given()
        .spec(spec)
        .body(load("json/UserRequest.json"))

        .when()
        .post("/users")

        .then()
        .log().ifValidationFails()
        .statusCode(201)
        .body("payload.firstName", equalTo(TEST))
        .body("payload.lastName", equalTo(TEST));
  }

  @Test
  void shouldCreateAccount() throws IOException {
    given()
        .spec(spec)
        .body(load("json/AccountRequest.json"))
        .pathParam("id", userId)

        .when()
        .post("/users/{id}/accounts")

        .then()
        .log().ifValidationFails()
        .statusCode(201)
        .body("payload.id", notNullValue());

  }

  @Test
  void shouldReturnError_WhenUserDoesNotExists() throws IOException {
    given()
        .spec(spec)
        .body(load("json/AccountRequest.json"))
        .pathParam("id", -10)

        .when()
        .post("/users/{id}/accounts")

        .then()
        .log().ifValidationFails()
        .statusCode(400)
        .body("statusCode", equalTo(2))
        .body("errorMessage", notNullValue());

  }

  @Test
  public void shouldReturnBalance() {
    given()
        .spec(spec)
        .pathParam("id", userId)

        .when()
        .get("/users/{id}/balance")

        .then()
        .log().ifValidationFails()
        .statusCode(200)
        .body("payload[0].accountNumber", notNullValue());
  }

}
