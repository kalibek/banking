package banking.integration;

import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.codec.Charsets;
import org.apache.commons.io.IOUtils;

public class IntegrationUtils {

  public static String load(String fileName) throws IOException {
    InputStream resourceAsStream = IntegrationUtils.class.getClassLoader()
        .getResourceAsStream(fileName);
    return IOUtils.toString(resourceAsStream, Charsets.UTF_8);
  }

}
