create table if not exists users
(
    id         bigint auto_increment,
    first_name varchar(255),
    last_name  varchar(255),
    created    timestamp not null default current_timestamp,
    primary key (id)
);

create table if not exists accounts
(
    id             bigint auto_increment,
    account_number uuid           not null default random_uuid(),
    amount         decimal(20, 2) not null,
    created        timestamp      not null default current_timestamp,
    owner_id       bigint         not null,
    foreign key (owner_id) references users (id),
    primary key (id)
);

create table if not exists operations
(
    id             bigint auto_increment,
    account_id     bigint         not null,
    amount         decimal(20, 2) not null,
    operation_type varchar(64)    not null,
    created        timestamp      not null default current_timestamp,
    foreign key (account_id) references accounts (id),
    primary key (id)
)

