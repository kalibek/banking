package banking.user;

import javax.inject.Inject;
import javax.sql.DataSource;
import lombok.SneakyThrows;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

public class UserRepo {

  private final QueryRunner runner;

  @Inject
  public UserRepo(DataSource dataSource) {
    this.runner = new QueryRunner(dataSource);
  }

  @SneakyThrows
  public long create(UserRequest userRequest) {
    return runner.insert("insert into users (first_name, last_name) values (?, ?)",
        new ScalarHandler<>(),
        userRequest.getFirstName(),
        userRequest.getLastName()
    );
  }

  @SneakyThrows
  public UserResponse find(long id) {
    BeanHandler<UserResponse> handler = new BeanHandler<>(UserResponse.class);
    return runner
        .query(
            "select id, first_name as firstName, last_name as lastName, created from users where id = ?",
            handler, id);
  }

  @SneakyThrows
  public boolean userExists(long id) {
    long count = runner.query("select count(*) from users where id = ?", new ScalarHandler<>(), id);
    return count > 0;
  }
}
