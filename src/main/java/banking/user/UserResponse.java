package banking.user;

import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserResponse {

  private long id;
  private String firstName;
  private String lastName;
  private Date created;
}
