package banking.user;


import static org.apache.commons.lang.StringUtils.isEmpty;

import banking.api.ApiException;
import banking.api.ErrorCode;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserService {

  private final UserRepo userRepo;

  @Inject
  public UserService(UserRepo userRepo) {
    this.userRepo = userRepo;
  }

  public UserResponse create(UserRequest userRequest) {
    if (isEmpty(userRequest.getFirstName()) && isEmpty(userRequest.getLastName())) {
      throw new ApiException(ErrorCode.E001);
    }
    long id = userRepo.create(userRequest);

    return userRepo.find(id);
  }

  public boolean userExists(long id) {
    return userRepo.userExists(id);
  }
}
