package banking.operation;

import java.math.BigDecimal;
import java.util.List;
import javax.inject.Inject;
import javax.sql.DataSource;
import lombok.SneakyThrows;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

public class OperationRepo {

  private final QueryRunner runner;

  @Inject
  public OperationRepo(DataSource dataSource) {
    this.runner = new QueryRunner(dataSource);
  }

  @SneakyThrows
  public void operation(long accountId, BigDecimal amount, OperationType operationType) {
    runner.update("insert into operations (account_id, amount, operation_type) values (?, ?, ?)",
        accountId, amount, operationType.toString());
  }

  @SneakyThrows
  public List<BalanceResponse> balanceByOwner(long ownerId) {
    BeanListHandler<BalanceResponse> handler = new BeanListHandler<>(BalanceResponse.class);
    return runner.query("select a.account_number accountNumber, sum(o.amount) accountBalance "
        + "from accounts a inner join operations o "
        + "on a.id = o.account_id where a.owner_id = ?", handler, ownerId);
  }

}
