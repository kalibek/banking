package banking.operation;

import java.math.BigDecimal;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BalanceResponse {

  private String accountNumber;
  private BigDecimal accountBalance;
}
