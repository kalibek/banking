package banking.operation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.UnaryOperator;
import javax.inject.Inject;

public class OperationService {

  private final OperationRepo operationRepo;

  private static final Map<OperationType, List<UnaryOperator<BigDecimal>>> rules = new HashMap<>();
  private static final BigDecimal DIVISOR = new BigDecimal(100);
  private static final BigDecimal MIN_FEE = new BigDecimal(5);
  private static final BigDecimal MAX_FEE = new BigDecimal(30);

  static {
    rules.put(OperationType.ACCOUNT_CREATION_FEE, List.of(
        fee -> fee.divide(DIVISOR, RoundingMode.HALF_UP),
        fee -> (fee.compareTo(MIN_FEE) < 0) ? MIN_FEE : fee,
        fee -> (fee.compareTo(MAX_FEE) > 0) ? MAX_FEE : fee
    ));
    rules.put(OperationType.OTHER, Collections.singletonList(fee -> BigDecimal.ZERO));
  }

  @Inject
  public OperationService(OperationRepo operationRepo) {
    this.operationRepo = operationRepo;

  }

  public BigDecimal getFee(OperationType operationType, BigDecimal amount) {
    List<UnaryOperator<BigDecimal>> rulesToApply = rules.get(operationType);
    BigDecimal fee = amount;
    for (UnaryOperator<BigDecimal> o : rulesToApply) {
      fee = o.apply(fee);
    }
    return fee;
  }

  public void operation(long accountId, BigDecimal amount, OperationType operationType) {
    operationRepo.operation(accountId, amount, operationType);
  }

  public List<BalanceResponse> balanceByOwner(long ownerId) {
    return operationRepo.balanceByOwner(ownerId);
  }

}
