package banking.operation;

public enum OperationType {
  ACCOUNT_CREATION_FEE, INIT_ACCOUNT, OTHER
}
