package banking;

import static spark.Spark.stop;

import banking.api.Router;
import banking.config.BindingModule;
import com.google.inject.Guice;

public class Application {


  public static void main(String[] args) {
    Guice.createInjector(new BindingModule()).getInstance(Router.class).run();
  }

  public static void stopServer() {
    stop();
  }

}
