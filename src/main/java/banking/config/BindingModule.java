package banking.config;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.name.Names;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BindingModule extends AbstractModule {

  @Override
  protected void configure() {
    Names.bindProperties(binder(), loadProperties());
    bind(DataSource.class).toProvider(H2DataSourceProvider.class).in(Scopes.SINGLETON);
  }

  private Properties loadProperties() {
    Properties properties = new Properties();
    try (InputStream resourceAsStream = BindingModule.class.getClassLoader()
        .getResourceAsStream("application.properties")) {
      properties.load(resourceAsStream);
    } catch (IOException e) {
      log.error("Cannot read properties file");
    }
    return properties;
  }
}
