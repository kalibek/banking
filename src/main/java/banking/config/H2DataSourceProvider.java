package banking.config;

import com.google.inject.Provider;
import com.google.inject.name.Named;
import javax.inject.Inject;
import javax.sql.DataSource;
import org.h2.jdbcx.JdbcDataSource;

public class H2DataSourceProvider implements Provider<DataSource> {

  private final String url;
  private final String username;
  private final String password;

  @Inject
  public H2DataSourceProvider(@Named("url") final String url,
      @Named("username") final String username,
      @Named("password") final String password) {
    this.url = url;
    this.username = username;
    this.password = password;
  }

  @Override
  public DataSource get() {
    final JdbcDataSource dataSource = new JdbcDataSource();
    dataSource.setURL(url);
    dataSource.setUser(username);
    dataSource.setPassword(password);
    return dataSource;
  }
}
