package banking.api;

import static spark.Spark.before;
import static spark.Spark.exception;
import static spark.Spark.get;
import static spark.Spark.path;
import static spark.Spark.port;
import static spark.Spark.post;

import banking.account.AccountRequest;
import banking.account.AccountService;
import banking.operation.OperationService;
import banking.user.UserRequest;
import banking.user.UserService;
import javax.inject.Inject;

public class Router {

  private final UserService userService;
  private final AccountService accountService;
  private final OperationService operationService;

  @Inject
  public Router(UserService userService, AccountService accountService,
      OperationService operationService) {
    this.userService = userService;
    this.accountService = accountService;
    this.operationService = operationService;
  }

  public void run() {
    port(8008);
    path("/api/v1/", () -> {
      before("/*", (req, res) -> res.type("application/json"));
      post("/users", (req, res) -> {
        res.status(201);
        UserRequest userRequest = Exchange.parseJson(req.body(), UserRequest.class);
        return Exchange.ok(userService.create(userRequest));
      });

      post("users/:id/accounts", (req, res) -> {
        res.status(201);
        AccountRequest accountRequest = Exchange.parseJson(req.body(), AccountRequest.class);
        long id = Long.parseLong(req.params(":id"));
        return Exchange.ok(accountService.create(id, accountRequest));
      });

      get("users/:id/balance", (req, res) -> {
        long id = Long.parseLong(req.params(":id"));
        return Exchange.ok(operationService.balanceByOwner(id));
      });

      exception(ApiException.class, (ex, req, res) -> {
        res.status(400);
        res.body(Exchange.exception(ex));
      });
    });
  }
}
