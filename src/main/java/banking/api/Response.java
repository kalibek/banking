package banking.api;

import static banking.api.ErrorCode.E000;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
class Response {

  private Integer statusCode;
  private String errorMessage;
  private Object payload;

  static Response ok(Object payload) {
    return Response.builder()
        .statusCode(E000.getCode())
        .errorMessage(E000.getMessage())
        .payload(payload).build();
  }

  static Response badRequest(ErrorCode errorCode) {
    return Response.builder()
        .statusCode(errorCode.getCode())
        .errorMessage(errorCode.getMessage())
        .build();
  }
}
