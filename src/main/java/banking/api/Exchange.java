package banking.api;

import com.google.gson.Gson;

public class Exchange {

  private static Gson gson = new Gson();

  public static <T> T parseJson(String json, Class<T> tClass) {
    return gson.fromJson(json, tClass);
  }

  public static String ok(Object payload) {
    Response response = Response.ok(payload);
    return gson.toJson(response);
  }

  public static String exception(ApiException ex) {
    Response response = Response.badRequest(ex.getErrorCode());
    return gson.toJson(response);
  }
}
