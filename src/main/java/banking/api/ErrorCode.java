package banking.api;

public enum ErrorCode {
  E000(0, "SUCCESS"),
  E001(1, "at least first name is required"),
  E002(2, "user does not exist"),
  E003(3, "initial amount cannot be null");

  private int code;
  private String message;

  ErrorCode(int code, String message) {
    this.code = code;
    this.message = message;
  }

  public int getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }

}
