package banking.account;

import javax.inject.Inject;
import javax.sql.DataSource;
import lombok.SneakyThrows;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

public class AccountRepo {

  private final QueryRunner runner;

  @Inject
  public AccountRepo(DataSource dataSource) {
    this.runner = new QueryRunner(dataSource);
  }

  @SneakyThrows
  public long create(long id, AccountRequest accountRequest) {
    return runner
        .insert("insert into accounts (owner_id, amount) values(?, ?)", new ScalarHandler<>(), id,
            accountRequest.getAmount());
  }

  @SneakyThrows
  public AccountResponse find(long accountId) {
    BeanHandler<AccountResponse> handler = new BeanHandler<>(AccountResponse.class);
    return runner.query(
        "select id, account_number as accountNumber, amount, created from accounts where id = ?",
        handler, accountId);
  }
}
