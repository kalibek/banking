package banking.account;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AccountResponse {

  private long id;
  private String accountNumber;
  private BigDecimal amount;
  private Date created;
}
