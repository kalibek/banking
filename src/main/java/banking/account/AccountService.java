package banking.account;

import banking.api.ApiException;
import banking.api.ErrorCode;
import banking.operation.OperationService;
import banking.operation.OperationType;
import banking.user.UserService;
import java.math.BigDecimal;
import javax.inject.Inject;

public class AccountService {

  private final AccountRepo accountRepo;
  private final UserService userService;
  private final OperationService operationService;

  @Inject
  public AccountService(AccountRepo accountRepo, UserService userService,
      OperationService operationService) {
    this.accountRepo = accountRepo;
    this.userService = userService;
    this.operationService = operationService;
  }

  public AccountResponse create(long id, AccountRequest accountRequest) {
    if (!userService.userExists(id)) {
      throw new ApiException(ErrorCode.E002);
    }

    if (accountRequest.getAmount() == null) {
      throw new ApiException(ErrorCode.E003);
    }

    long accountId = accountRepo.create(id, accountRequest);

    BigDecimal fee = operationService
        .getFee(OperationType.ACCOUNT_CREATION_FEE, accountRequest.getAmount());

    operationService.operation(accountId, accountRequest.getAmount(), OperationType.INIT_ACCOUNT);
    operationService.operation(accountId, fee.negate(), OperationType.ACCOUNT_CREATION_FEE);

    return accountRepo.find(accountId);
  }
}
